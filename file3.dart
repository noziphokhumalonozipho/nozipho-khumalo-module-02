class App {

    String name_of_app;
    String category;
    String developer;
    String year;

    App(this.name_of_app, this.category, this.developer, this.year); 

    String sentence() {
        return "name: ${this.name_of_app}\n category: ${this.category}\n developer: ${this.developer}\n year: ${this.year}";
    }    

    String sentence_UPPERCASE() {
        return "name: ${this.name_of_app}\n category: ${this.category}\n developer: ${this.developer}\n year: ${this.year}"
          .toUpperCase();
    }
    
}

main() {

    var app = App("FNB", "Best Consumer Solution", "winner", "2012"); 
       
    // --- Normal ---
    print(app.sentence());

    // --- Capital Letters ---
    print(app.sentence_UPPERCASE());

}
